FROM nginx:stable

WORKDIR /srv/digistrip

COPY . /srv/digistrip/

COPY nginx.conf  /etc/nginx/conf.d/default.conf
RUN apt-get update && apt-get install -y php-fpm php-sqlite3

EXPOSE 80

