// Structure de la galerie
	// Nom du dossier, Nom affiché, Crédits auteur, Thèmes...
	fonds = [
		['Abel_Bellina','Abel & Bellina','Abel_Bellina'],
		['Ada','Ada & Zangemann','Ada_Zangemann'],
		['Jous','Le Monde des Jous','Jous'],
		['École','École','Bing'],
		['Fantastique','Fantastique','Bing'],
		['Hôpital','Hôpital','Bing'],
		['Maison','Maison','Bing'],
		['Montagne','Montagne','Bing'],
		['Plage','Plage','Bing'],
		["Primtux","Primtux",'Primtux'],
		['Restaurant','Restaurant','Bing'],
		['Science_fiction','Science fiction','Bing'],
		['Travail','Travail','Bing'],
		['Village','Village','Bing'],
		['Ville','Ville','Bing'],
	]
	// Nom du dossier, Nom affiché, Crédits auteur, Thèmes...
	personnages = [
		['Abel','Abel','Abel_Bellina','Série 1','Série 2','Série 3','Série 4'],
		['Abel_Bellina','Abel & Bellina','Abel_Bellina'],
		['Abby','Abby The Pup','Abby'],
		['Ada_Zangemann','Ada & Zangemann','Ada_Zangemann'],
		["Alexia","Alexia",'Ramya_Mylavarapu',"Doigt levé","Porte un livre"],
		["Benjamin","Benjamin",'svgrepo',"Face","Trois-quarts"],
		['Bellina','Bellina','Abel_Bellina','Série 1','Série 2','Série 3','Série 4'],
		['Brigit_Komit','Brigit & Komit','Brigit_Komit'],
		["Caroline","Caroline","svgrepo","Face","Trois-quarts"],
		["Clémence","Clémence","Ramya_Mylavarapu","Doigt levé","Porte un livre"],
		["Divers","Divers","svgrepo"],
		["Jous","Jous","Jous"],
		["Léopold","Léopold",'Devarani_B',"Doigt levé","Tient une baguette"],
		["Lily","Lily",'Bing'],
		["Manon","Manon",'Bing'],
		["Mathilde","Mathilde",'Bing'],
		["Max","Max",'Bing'],
		["Ours","Ours",'Bing'],
		["Primtux","Primtux",'Primtux'],
		["Tasse","Tasse",'Ramya_Mylavarapu',"Doigt pointé","Bras écartés","Journal"],
		["Tux","Tux",'Tux']
	]
	// Nom du dossier, Nom affiché, Crédits auteur, Thèmes...
	objets = [
		['Abel_Bellina','Abel & Bellina','Abel_Bellina']
	]
	// Noms polices
	polices = [
		"IndieFlower","FuntypeRegular","ThatNogoFontCasual","Kaph","Teleindicadore","Segment"
	]
	// Référence, texte à afficher
	credits = [
		['Abel_Bellina','Dessins Abel & Bellina par Odysseus, licence Art Libre'],
		['Abby','Dessins Feelings par Kulsoom Ayyaz, licence Public Domain'],
		['Jous','Dessins des Jous par Arnaud Champollion, licence CC-BY-SA-4.0'],
		['Ada_Zangemann','Dessins Ada & Zangemann par Sandra Brandstätter, licence CC-BY-SA-3.0-DE'],
		['Brigit_Komit','Dessins de Brigit & Komit par Juliette Taka, licence CC BY.'],  
		["svgrepo","Dessins SVGRepo"],
		["Bing","Images générées par IA sur Bing"],
		["Ramya_Mylavarapu","Dessins par Ramya Mylavarapu sur gramener.com/comicgen, licence CC0"],
		['Devarani_B','Dessins par Devarani B sur gramener.com/comicgen, licence CC0'],
		['Tux',"Dessin de Tux original par Larry Ewing, version crystal par Everaldo Coelho"],
		["IndieFlower","Police de caractère Indie Flower par Kimberly Geswein, licence OFL"],
		["FuntypeRegular","Police de caractère Fun Type Regular par Frank Baranowski, licence OFL"],
		["ThatNogoFontCasual","Police de caractère That Nogo Font Casual par Kimberly Geswein, licence OFL"],
		["Kaph","Police de caractère Kaph par GGBotNet, licence OFL"],
		["Teleindicadore","Police de caractère Tele Indicadore sous Public Licence"],
		["Segment","Police de caractère Segment par Paul Flo Williams, licence OFL"],
		["Primtux","Dessins des personnages de Primtux par Romain Ronflette, licence CC BY 4.0"]
	]

	// Liste des fonds
	liste_fonds = {
		"Abel_Bellina": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg",
				"09.jpeg",
				"10.jpeg",
				"11.jpeg",
				"12.jpeg",
				"13.jpeg",
				"14.jpeg",
				"15.jpeg",
				"16.jpeg",
				"17.jpeg"
			]
		},
		"Ada": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg"
			]
		},
		"École": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg",
				"09.jpeg",
				"10.jpeg",
				"11.jpeg",
				"12.jpeg"
			]
		},
		"Fantastique": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg",
				"09.jpeg",
				"10.jpeg",
				"11.jpeg",
				"12.jpeg",
				"13.jpeg"
			]
		},
		"Hôpital": {
			1: [
				"31.jpeg",
				"32.jpeg",
				"33.jpeg",
				"34.jpeg",
				"35.jpeg",
				"36.jpeg",
				"37.jpeg"
			]
		},
		"Jous": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg"
			]
		},
		"Maison": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg"
			]
		},
		"Montagne": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg"
			]
		},
		"Plage": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg",
				"09.jpeg"
			]
		},
		"Primtux": {
			1: [
				"01.jpeg",
				"02.jpeg"
			]
		},
		"Restaurant": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg"
			]
		},
		"Science_fiction": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg",
				"09.jpeg",
				"10.jpeg",
				"11.jpeg",
				"12.jpeg",
				"13.jpeg",
				"14.jpeg",
				"15.jpeg",
				"16.jpeg",
				"17.jpeg"
			]
		},
		"Travail": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg",
				"09.jpeg"
			]
		},
		"Village": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg",
				"09.jpeg",
				"10.jpeg",
				"11.jpeg"
			]
		},
		"Ville": {
			1: [
				"01.jpeg",
				"02.jpeg",
				"03.jpeg",
				"04.jpeg",
				"05.jpeg",
				"06.jpeg",
				"07.jpeg",
				"08.jpeg"
			]
		},
	}

	// Liste des personnages
	liste_personnages = {
		"Abby": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg"
			]
		},
		"Abel": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png",
				"perso09.png",
				"perso10.png",
				"perso11.png",
				"perso12.png",
				"perso13.png",
				"perso14.png",
				"perso15.png",
				"perso16.png",
				"perso17.png",
				"perso18.png",
				"perso19.png"
			],
			2: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png",
				"perso09.png",
				"perso10.png",
				"perso11.png",
				"perso12.png",
				"perso13.png",
				"perso14.png",
				"perso15.png",
				"perso16.png",
				"perso17.png",
				"perso18.png",
				"perso19.png"
			],
			3: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png",
				"perso09.png",
				"perso10.png",
				"perso11.png",
				"perso12.png",
				"perso13.png",
				"perso14.png",
				"perso15.png",
				"perso16.png",
				"perso17.png"
			],
			4: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png"
			]
		},
		"Abel_Bellina": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png"
			]
		},
		"Ada_Zangemann": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png",
				"perso09.png"
			]
		},
		"Alexia": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg",
				"perso11.svg",
				"perso12.svg",
				"perso13.svg",
				"perso14.svg",
				"perso15.svg",
				"perso16.svg",
				"perso17.svg",
				"perso18.svg",
				"perso19.svg",
				"perso20.svg"
			],
			2: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg",
				"perso11.svg",
				"perso12.svg",
				"perso13.svg",
				"perso14.svg",
				"perso15.svg",
				"perso16.svg",
				"perso17.svg",
				"perso18.svg",
				"perso19.svg",
				"perso20.svg"
			]
		},
		"Bellina": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png",
				"perso09.png",
				"perso10.png",
				"perso11.png",
				"perso12.png",
				"perso13.png",
				"perso14.png",
				"perso15.png",
				"perso16.png",
				"perso17.png",
				"perso18.png",
				"perso19.png",
				"perso20.png"
			],
			2: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png",
				"perso09.png",
				"perso10.png",
				"perso11.png",
				"perso12.png",
				"perso13.png",
				"perso14.png",
				"perso15.png",
				"perso16.png",
				"perso17.png",
			],
			3: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png",
				"perso09.png",
				"perso10.png",
				"perso11.png",
				"perso12.png",
				"perso13.png",
				"perso14.png",
				"perso15.png",
				"perso16.png",
				"perso17.png"
			],
			4: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png"
			]
		},
		"Benjamin": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg"
			],
			2: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg"
			]
		},
		"Brigit_Komit": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg"
			]
		},
		"Caroline": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg"
			],
			2: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg"
			]
		},
		"Clémence": {
			1: [
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg",
				"perso12.svg",
				"perso13.svg",
				"perso14.svg",
				"perso16.svg",
				"perso17.svg",
				"perso18.svg",
				"perso19.svg",
				"perso20.svg"
			],
			2: [
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg",
				"perso11.svg",
				"perso12.svg",
				"perso13.svg",
				"perso14.svg",
				"perso16.svg",
				"perso17.svg",
				"perso18.svg",
				"perso20.svg"
			]
		},
		"Divers": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg",
				"perso11.svg",
				"perso12.svg",
				"perso13.svg",
				"perso14.svg",
				"perso15.svg",
				"perso16.svg",
				"perso17.svg",
				"perso18.svg"
			]
		},
		"Jous": {
			1: [
				"perso01.svg",
				"perso02.png",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg"
			]
		},
		"Léopold": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg",
				"perso11.svg",
				"perso12.svg",
				"perso13.svg"
			],
			2: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg",
				"perso11.svg",
				"perso12.svg"
			]
		},
		"Lily": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png"
			]
		},
		"Manon": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png"
			]
		},
		"Mathilde": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png"
			]
		},
		"Max": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png"
			]
		},
		"Ours": {
			1: [
				"perso01.png",
				"perso02.png",
				"perso03.png",
				"perso04.png",
				"perso05.png",
				"perso06.png",
				"perso07.png",
				"perso08.png",
				"perso09.png",
				"perso10.png",
				"perso11.png",
				"perso12.png",
				"perso13.png"
			]
		},
		"Primtux": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg"
			]
		},
		"Tasse": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg"
			],
			2: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg",
				"perso08.svg",
				"perso09.svg",
				"perso10.svg",
				"perso11.svg"
			],
			3: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg",
				"perso04.svg",
				"perso05.svg",
				"perso06.svg",
				"perso07.svg"
			]
		},
		"Tux": {
			1: [
				"perso01.svg",
				"perso02.svg",
				"perso03.svg"
			]
		}
	}

	// Liste des objets
	liste_objets = {
		"Abel_Bellina": {
			1: [
				"objet01.png",
				"objet02.png",
				"objet03.png",
				"objet04.png",
				"objet05.png",
				"objet06.png",
				"objet07.png",
				"objet08.png",
				"objet09.png",
				"objet10.png",
				"objet11.png",
				"objet12.png",
				"objet13.png",
			]
		}
	}
