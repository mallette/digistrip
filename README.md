# Digistrip

Digistrip est une application en ligne permettant de réaliser une bande dessinée à partir d'une bibliothèque d'images.

Elle est publiée sous licence GNU GPLv3.
Sauf la fonte HKGrotesk (Sil Open Font Licence 1.1)

Digistrip est un fork du projet Auto BD (https://forge.apps.education.fr/educajou/autobd) créé par Arnaud Champollion avec l'aide de Cyril Laconelli, de Cédric Eyssette et de Mathieu-Gilbert Degrange.

### Variables d'environnement (fichier .env à créer à la racine du dossier)
```
AUTHORIZED_DOMAINS (* ou liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule)
```

### Serveur PHP nécessaire pour l'API
```
php -S localhost:8000 (pour le développement uniquement)
```

### Démo
https://ladigitale.dev/digistrip/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/