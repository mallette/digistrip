<?php

session_start();

require 'headers.php';

if (!empty($_FILES['blob']) && !empty($_POST['bd'])) {
	$fichier = $_POST['fichier'];
	$bd = $_POST['bd'];
	$extension = pathinfo($fichier, PATHINFO_EXTENSION);
	if (!file_exists('../fichiers/' . $bd)) {
		mkdir('../fichiers/' . $bd, 0775, true);
	}
	$chemin = '../fichiers/' . $bd . '/' . $fichier;
	if (move_uploaded_file($_FILES['blob']['tmp_name'], $chemin)) {
		echo 'fichier_importe';
	} else {
		echo 'erreur';
	}
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
