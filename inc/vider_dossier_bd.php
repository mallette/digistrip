<?php

session_start();

require 'headers.php';

if (!empty($_POST['bd'])) {
	$bd = $_POST['bd'];
	if (file_exists('../fichiers/' . $bd)) {
		$fichiers = glob('../fichiers/' . $bd . '/' . '*.*');
		foreach ($fichiers as $f) {
			unlink($f);
		}
	}
	echo 'dossier_vide';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
