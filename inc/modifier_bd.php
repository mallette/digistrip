<?php

session_start();

require 'headers.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST['bd']) && !empty($_POST['donnees'])) {
	require 'db.php';
	$reponse = '';
	$bd = $_POST['bd'];
	if (isset($_SESSION['digistrip'][$bd]['reponse'])) {
		$reponse = $_SESSION['digistrip'][$bd]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digistrip_bd WHERE url = :url');
	if ($stmt->execute(array('url' => $bd))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else if ($resultat[0]['reponse'] === $reponse) {
			$donnees = $_POST['donnees'];
			$stmt = $db->prepare('UPDATE digistrip_bd SET donnees = :donnees WHERE url = :url');
			if ($stmt->execute(array('donnees' => json_encode($donnees), 'url' => $bd))) {
				echo 'bd_modifiee';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
