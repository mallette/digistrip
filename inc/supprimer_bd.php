<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$bd = $_POST['id'];
	if (isset($_SESSION['digistrip'][$bd]['reponse'])) {
		$reponse = $_SESSION['digistrip'][$bd]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digistrip_bd WHERE url = :url');
	if ($stmt->execute(array('url' => $bd))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else if ($resultat[0]['reponse'] === $reponse) {
			$stmt = $db->prepare('DELETE FROM digistrip_bd WHERE url = :url');
			if ($stmt->execute(array('url' => $bd))) {
				if (file_exists('../fichiers/' . $bd)) {
					supprimer('../fichiers/' . $bd);
				}
				echo 'bd_supprimee';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

function supprimer ($path) {
	if (is_dir($path) === true) {
		$files = array_diff(scandir($path), array('.', '..'));
		foreach ($files as $file) {
			supprimer(realpath($path) . '/' . $file);
		}
		return rmdir($path);
	} else if (is_file($path) === true) {
		return unlink($path);
	}
	return false;
}

?>
