<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digistrip'][$id]['reponse'])) {
		$reponse = $_SESSION['digistrip'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digistrip_bd WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($bd = $stmt->fetchAll()) {
			$admin = false;
			if (count($bd, COUNT_NORMAL) > 0 && $bd[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $bd[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digistrip'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digistrip'][$id]['digidrive'];
			}
			$date = date('Y-m-d H:i:s');
			$vues = 0;
			if ($bd[0]['vues'] !== '') {
				$vues = intval($bd[0]['vues']);
			}
			if ($admin === false) {
				$vues = $vues + 1;
			}
			$stmt = $db->prepare('UPDATE digistrip_bd SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
			if ($stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $id))) {
				echo json_encode(array('titre' => $bd[0]['titre'], 'donnees' => $donnees, 'vues' => $vues, 'admin' => $admin, 'digidrive' => $digidrive));
			} else {
				echo 'erreur';
			}
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
